var cytoscape = require('cytoscape')
var edgehandles = require('cytoscape-edgehandles')
const iro = require('@jaames/iro')

cytoscape.use(edgehandles)

function rgb2hex(rgb) {
  if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb

  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/)
  function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2)
  }
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3])
}

$(function () {

  var cy = cytoscape({
    container: $('#cy'),

    elements: [ // list of graph elements to start with
      { // node a
        data: { id: 'a' }
      },
      { // node b
        data: { id: 'b' }
      },
      { // edge ab
        data: { id: 'ab', source: 'a', target: 'b' }
      }
    ],
    style: [
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'target-arrow-shape': 'none'
        }
      },

      // some style for the extension

      {
        selector: '.eh-handle',
        style: {
          'background-color': 'black',
          'width': 12,
          'height': 12,
          'shape': 'ellipse',
          'overlay-opacity': 0,
          'border-width': 12, // makes the handle easier to hit
          'border-opacity': 0
        }
      },

      {
        selector: '.eh-hover',
        style: {
          'background-color': 'red'
        }
      },

      {
        selector: '.eh-source',
        style: {
          'border-width': 2,
          'border-color': 'red'
        }
      },

      {
        selector: '.eh-target',
        style: {
          'border-width': 2,
          'border-color': 'red'
        }
      },

      {
        selector: '.eh-ghost-edge.eh-preview-active',
        style: {
          'opacity': 0
        }
      }
    ]

  })
  cy.reset()
  cy.resize()

  var eh = cy.edgehandles({
    edgeType: function (sourceNode, targetNode) {
      return sourceNode.edgesWith(targetNode).empty() ? 'flat' : null
    },
  })

  var nodeMode = true
  var edgeMode = false
  var delMode = false
  var constMode = false


  $('#cy').mouseenter(function () {
    if ($('#nodeMode').hasClass('active')) {
      eh.disableDrawMode()
      eh.disable()
      nodeMode = true
    } else nodeMode = false

    if ($('#edgeMode').hasClass('active')) {
      eh.enableDrawMode()
      eh.enable()
      edgeMode = true
    } else edgeMode = false

    if ($('#delMode').hasClass('active')) {
      eh.disableDrawMode()
      eh.disable()
      delMode = true
    } else delMode = false

    if ($('#constMode').hasClass('active')) {
      eh.disableDrawMode()
      eh.disable()
      constMode = true
    } else constMode = false
  })


  cy.on("tap", function (e) {
    if (nodeMode === true) {
      cy.add([{
        group: "nodes",
        id: "testid",
        renderedPosition: {
          x: e.renderedPosition.x,
          y: e.renderedPosition.y,
        }
      }])
    }
  })


  cy.on("tap", "node", function (e) {
    if (delMode === true) {
      cy.remove(cy.$id(this.id()))
    }

  })

  cy.on("tap", "edge", function (e) {
    if (delMode === true) {
      cy.remove(cy.$id(this.id()))
    }
  })

  $(".palette").click(function (e) {
    if (constMode === true && !retrieveColors(cy.$(':selected').id()).includes($(this).css('background-color').replace(/\s+/g, ''))) {
      var numOfColor = 0
      var selectedStyle = cy.style().json()
      var selectedID = '#' + cy.$(':selected').id()
      var paletteColor = rgb2hex($(this).css('background-color'))
      var hasColor = false
      var colorIndex = 0
      var tempJson = {}
      for (var i = 0; i < selectedStyle.length; i++) {
        // Cek if punya warna already
        if (selectedStyle[i]["selector"] === selectedID) {
          hasColor = true
          colorIndex = i
          break
        }
      }
      if (hasColor === true) {
        // Itung jumlah warna yang ada
        for (var k = colorIndex; k < selectedStyle.length; k++) {
          if ('pie-' + (numOfColor + 1) + '-background-color' in selectedStyle[k]['style']) {
            numOfColor += 1
          }
        }
        var newSize = 100.0 / (numOfColor + 1)
        // New size untuk warna yang ada
        for (var j = 1; j <= numOfColor; j++) {
          tempJson = {}
          tempJson['pie-' + j + '-background-size'] = newSize + '%'
          cy.style().selector(selectedID).style(tempJson).update()
        }
        // make new color
        tempJson = {}
        tempJson['pie-' + (numOfColor + 1) + '-background-color'] = paletteColor
        tempJson['pie-' + (numOfColor + 1) + '-background-size'] = newSize + '%'
        cy.style().selector(selectedID)
          .style(tempJson).update()
      }
      else {
        cy.style().selector(selectedID).style({
          'width': '60px',
          'height': '60px',
          'pie-size': '80%',
          'pie-1-background-color': paletteColor,
          'pie-1-background-size': '100%',
        }).update()
      }
    }
  })

  $('#but').click(function (e) {
    e.preventDefault()
    getCleanJSON()
    var nodeData = getCleanJSON()
    $.ajax({
      type: "POST",
      url: "/api/perform-csp",
      data: {
        data: nodeData,
      },
      dataType: "json",
      success: function (result) {
        resArray = Object.keys(result)
        if (resArray.length === 0) {
          $('result').text('Solusi tidak ditemukan')
        }
        else {
          $('result').text('Solusi ditemukan')
          applyResult(result)
        }
      },
      error: function (result) {
        $('#result').text('Something is wrong with the server')
      }
    })
  })

  // Retrieve color from node
  function retrieveColors(id) {
    var selectedStyle = cy.style().json()
    var selectedID = '#' + id
    var colorIndex = 0
    var colorArray = new Array()
    var numOfColor = 0
    for (var i = 0; i < selectedStyle.length; i++) {
      // Cari index warna pertama
      if (selectedStyle[i]["selector"] === selectedID) {
        colorIndex = i
        break
      }
    }

    if (colorIndex !== 0) {
      for (var k = colorIndex; k < selectedStyle.length; k++) {
        if ('pie-' + (numOfColor + 1) + '-background-color' in selectedStyle[k]['style']) {
          colorArray.push(selectedStyle[k]['style']['pie-' + (numOfColor + 1) + '-background-color'])
          numOfColor += 1
        }
      }
    }
    return colorArray
  }

  function retrieveColorsHex(id) {
    var selectedStyle = cy.style().json()
    var selectedID = '#' + id
    var colorIndex = 0
    var colorArray = new Array()
    var numOfColor = 0
    for (var i = 0; i < selectedStyle.length; i++) {
      // Cari index warna pertama
      if (selectedStyle[i]["selector"] === selectedID) {
        colorIndex = i
        break
      }
    }

    if (colorIndex !== 0) {
      for (var k = colorIndex; k < selectedStyle.length; k++) {
        if ('pie-' + (numOfColor + 1) + '-background-color' in selectedStyle[k]['style']) {
          colorArray.push(rgb2hex(selectedStyle[k]['style']['pie-' + (numOfColor + 1) + '-background-color']))
          numOfColor += 1
        }
      }
    }
    return colorArray
  }


  var colorPicker = new iro.ColorPicker('#iroCP')
  colorPicker.resize($('#colorpicker').width() / 2)

  var domain = new Array()

  $('#colorButton').click(function () {
    if (!domain.includes(colorPicker.color.hexString)) {
      domain.push(colorPicker.color.hexString)
    }
    for (var i = 0; i < domain.length; i++) {
      $("#d" + i).css('background-color', domain[i])
    }
  })

  $(window).resize(function () {
    colorPicker.resize($('#colorpicker').width() / 2)
  })

  function getCleanJSON() {
    var CSPObject = new Object()

    var nodes = cy.json().elements.nodes
    var arr = new Array()

    for (var key in nodes) {
      var obj = new Object()
      obj.id = nodes[key].data.id
      obj.constraint = retrieveColorsHex(obj.id)
      arr.push(obj)
    }

    var arrOfArr = new Array()
    var edges = cy.json().elements.edges
    for (var key in edges) {
      var tempArr = new Array()
      tempArr.push(edges[key].data.source)
      tempArr.push(edges[key].data.target)
      arrOfArr.push(tempArr)
    }

    CSPObject.nodes = arr
    CSPObject.edges = arrOfArr
    CSPObject.domain = domain

    console.log(JSON.stringify(CSPObject))
    return CSPObject
  }

  function applyResult(json) {
    for (var item in json) {
      cy.style().selector(item).style({
        'background-color': json[item]
      }).update()
    }
  }
})