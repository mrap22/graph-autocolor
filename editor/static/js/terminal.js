var xterm = require('xterm')

$(function () {
    var term = new xterm.Terminal({ rows: 5 })
    term.open(document.getElementById('terminal'))
    term.write("hello")
    term.onScroll(term.scrollLines(1))
})