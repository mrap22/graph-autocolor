from processor.utils import CSP, Backtrack, BacktractingSearch
from utils import CSP, Backtrack, BacktractingSearch, json_to_graph

if __name__ == "__main__":
    X = ['WA','NT','Q','NSW','V','SA','T']
    D = {'WA': [1, 2, 3], 'NT': [1, 2, 3], 'Q': [1, 2, 3], 'NSW': [1, 2, 3], 'V': [1, 2, 3], 'SA': [1, 2, 3], 'T': [1, 2, 3]}
    C = [('WA','NT'),('WA','SA'),('NT','SA'),('NT','Q'),('SA','Q'),('SA','V'),('Q','NSW'),('NSW','V')]

    csp = CSP(X,D,C)
    res = BacktractingSearch(csp)
    print(res)

    X = ['WA','NT','Q','NSW','V','SA','T']
    D = {'WA': [1], 'NT': [1], 'Q': [1], 'NSW': [1], 'V': [1], 'SA': [1], 'T': [1]}
    C = [('WA','NT'),('WA','SA'),('NT','SA'),('NT','Q'),('SA','Q'),('SA','V'),('Q','NSW'),('NSW','V')]

    csp = CSP(X,D,C)
    res = BacktractingSearch(csp)
    print(res)

    X = ['1','2','3','4']
    D = {'1': [1,2,3], '2': [2,1,3], '3': [3,2,1], '4': [1,2,3]}
    C = [('1','4'),('2','4'),('3','4')]

    csp = CSP(X,D,C)
    res = BacktractingSearch(csp)
    print(res)
    data = '''
    {
    "nodes": [
      {
        "data": {
          "id": "a"
        },
        "position": {
          "x": 426.5,
          "y": 645
        },
        "group": "nodes",
        "removed": false,
        "selected": false,
        "selectable": true,
        "locked": false,
        "grabbable": true,
        "pannable": false,
        "classes": ""
      },
      {
        "data": {
          "id": "b"
        },
        "position": {
          "x": 1279.5,
          "y": 645
        },
        "group": "nodes",
        "removed": false,
        "selected": false,
        "selectable": true,
        "locked": false,
        "grabbable": true,
        "pannable": false,
        "classes": ""
      }
    ],
    "edges": [
      {
        "data": {
          "id": "ab",
          "source": "a",
          "target": "b"
        },
        "position": {
          "x": 0,
          "y": 0
        },
        "group": "edges",
        "removed": false,
        "selected": false,
        "selectable": true,
        "locked": false,
        "grabbable": true,
        "pannable": true,
        "classes": ""
      }
    ]
  }'''
    print(json_to_graph(data))
