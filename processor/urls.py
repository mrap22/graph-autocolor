from django.urls import path

from processor.views import CSPView

urlpatterns = [
    path('perform-csp/', CSPView.as_view()),
]