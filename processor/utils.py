import json

class CSP:
    def __init__(self, X, D, C):
        self.variable = X
        self.domain = D
        self.constraint = C

    def AssingmentIsCompelete(self, assignment: dict):
        for var in self.variable:
            if var not in assignment.keys():
                return False
        return True

    def SelectUnassignedVariable(self, assignment: dict):
        # TODO: Implement heuristic function i.e. MRV or degree heuristic
        for var in self.variable:
            if var not in assignment.keys():
                return var

    def GetDomain(self, variable):
        # TODO: implement domain ordering
        return self.domain.get(variable)

    def RemoveDomain(self, variable):
        return self.domain.get(variable).pop()

    def CheckConsistency(self, variable, value, assignment):
        consistent = True
        for c in self.GetConstraintForVariable(variable):
            # untuk setiap constraint cek apakah variable lain sudah di assign
            other_variable = c[0] if c.index(variable) == 1 else c[1]
            if other_variable in assignment.keys():
                if assignment[other_variable] == value:
                    consistent = False
        return consistent

    def GetConstraintForVariable(self, variable):
        return [c for c in self.constraint if variable in c]


def BacktractingSearch(csp):
    return Backtrack({}, csp)


def Backtrack(assignment, csp):
    if csp.AssingmentIsCompelete(assignment):
        return assignment
    u_var = csp.SelectUnassignedVariable(assignment)
    for value in csp.GetDomain(u_var):
        if csp.CheckConsistency(u_var, value, assignment):
            assignment[u_var] = value
            result = Backtrack(assignment, csp)
            if result != False:
                return result
            del assignment[u_var]

def json_to_graph(json_string):
    # returns X, C
    data = json.loads(json_string)
    X = []
    C = []
    for node in data["nodes"]:
        X.append(node['data']['id'])
    for edge in data['edges']:
        C.append((edge['data']['source'], edge['data']['target']))

    return X, C