from typing import Callable, Optional

reporter = []
counter = 0

def report(msg: str):
    global counter, reporter
    counter += 1
    reporter.append({
        "message": f"{counter} . {msg}"
    })


def is_complete(assignments: dict):
    """
        Check if all variable has been assigned a value.

        Parameters :
            assignments (dict) : A dictionary of all variable (string) and it's assigned value (int).
            e.g. 
                assignments = {'a':1, 'b':2, 'c':None}

        Output :
            complete (boolean) : Initialy true, false if there's a variable with assigned value of None.

    """
    complete = True
    for i in assignments.values():
        if i is None:
            complete = False
            break
    return complete


def select_unassigned_variable(variables: list, assignments: dict, constraints: Optional[list] = None, custom_algorithm: Optional[Callable[[list, dict, list], str]] = None):
    """
        Select an unassigned variable from variable list by checking it's assignment.

        Parameters :
            variables (list) : A list of all variable (string).
            e.g. 
                variables = ['a','b','c']

            assignments (dict) : A dictionary of all variable (string) and it's assigned value (int).
            e.g. 
                assignments = {'a':1, 'b':2, 'c':None}

            constraint (list) : A list of tupple of two constraint variable. 
                Not used by default, may be used by alternative alorigthm. Default to None
            e.g. 
                constraint = [('a','b'),('b','c')]

            custom_algorithm (callable) : A function that take variable (list), assignment (dict) and constraint (list). Return (Boolean)
                Drop-in replacement for this function
            e.g. 
                MRV, degree_heuristic

        Output :
            var (string) : a variable with no assignment.
            i.e. 
                assignments[var] is None

    """
    selected_variable = None
    if custom_algorithm is not None:
        selected_variable = custom_algorithm(
            variables, assignments, constraints)
    else:
        for variable in variables:
            if assignments[variable] is None:
                selected_variable = variable
                break
    report(f"variable {selected_variable} selected")
    return selected_variable


def is_consistent(assignments: dict, constraints: list):
    """
        Check if variable assignment doesn't violate constraints.
        For each constraint unpack it's variables, get variables assigned value, 
            if both assigned value is not None and both value are the same then the assigment is not valid.

        Parameters :
            assignments (dict) : A dictionary of all variable (string) and it's assigned value (int).
            e.g. 
                assignments = {'a':1, 'b':2, 'c':None}

            constraint (list) : A list of tupple of two constraint variable. 
                Not used by default, may be used by alternative alorigthm. Default to None
            e.g. 
                constraint = [('a','b'),('b','c')]

        Output :
            consistent (boolean) : Initialy true, false if there's an assignment that violated constraint.

    """
    consistent = True
    for var_a, var_b in constraints:
        val_a = assignments[var_a]
        val_b = assignments[var_b]
        if (val_a is not None and val_b is not None and val_a == val_b):
            consistent = False
            report(
                f"assigned value {val_a} for variable {var_a} and value {val_b} for variable {var_b} violated constraint")
            break
    return consistent


def init_assignment(variables: list):
    """
        Return initial assignment of all variable.

        Parameters :
            variables (list) : A list of all variable (string).
            e.g. 
                variables = ['a','b','c']

        Output :
            assignment (dict) : Initial variable assigned value of None.

    """
    assignment = {}
    for variable in variables:
        assignment[variable] = None
    return assignment


def recursive_backtracking(assignment: dict, csp: dict):
    if is_complete(assignment):
        return assignment
    var = select_unassigned_variable(csp["variables"], assignment)
    for value in csp["domains"][var]:
        assignment[var] = value
        report(f"assigned value {value} for variable {var}")
        if is_consistent(assignment, csp["constraints"]):
            report(f"assigned value {value} for variable {var} is valid")
            result = recursive_backtracking(assignment, csp)
            if result != None:
                return result
        report(f"assigned value {value} for variable {var} is not valid")
        assignment[var] = None
    return None


# my_csp = {
#     "variables": ["WA", "NT", "Q", "NSW", "V", "SA", "T"],
#     "domains": {'WA': {1, 2, 3}, 'NT': {1, 2, 3}, 'Q': {1, 2, 3}, 'NSW': {1, 2, 3}, 'V': {1, 2, 3}, 'SA': {1, 2, 3}, 'T': {1, 2, 3}},
#     "constraints": [('WA', 'NT'), ('WA', 'SA'), ('NT', 'SA'), ('NT', 'Q'),
#                     ('SA', 'Q'), ('SA', 'V'), ('Q', 'NSW'), ('NSW', 'V')]
# }
# reporter = []
# counter = 0

# result = recursive_backtracking(init_assignment(my_csp["variables"]), my_csp)

# print(result)
# for i in reporter:
#     print(i["message"])

# X = ['WA', 'NT', 'Q', 'NSW', 'V', 'SA', 'T']
# D = {'WA': [1, 2, 3], 'NT': [1, 2, 3], 'Q': [1, 2, 3], 'NSW': [
#     1, 2, 3], 'V': [1, 2, 3], 'SA': [1, 2, 3], 'T': [1, 2, 3]}
# C = [('WA', 'NT'), ('WA', 'SA'), ('NT', 'SA'), ('NT', 'Q'),
#      ('SA', 'Q'), ('SA', 'V'), ('Q', 'NSW'), ('NSW', 'V')]
# my_csp = {
#     "variables": X,
#     "domains": D,
#     "constraints": C
# }
reporter = []
counter = 0

# result = recursive_backtracking(init_assignment(my_csp["variables"]), my_csp)

# print(result)
# for i in reporter:
#     print(i["message"])

# X = ['WA', 'NT', 'Q', 'NSW', 'V', 'SA', 'T']
# D = {'WA': [1], 'NT': [1], 'Q': [1], 'NSW': [1], 'V': [1], 'SA': [1], 'T': [1]}
# C = [('WA', 'NT'), ('WA', 'SA'), ('NT', 'SA'), ('NT', 'Q'),
#      ('SA', 'Q'), ('SA', 'V'), ('Q', 'NSW'), ('NSW', 'V')]
# my_csp = {
#     "variables": X,
#     "domains": D,
#     "constraints": C
# }
# reporter = []
# counter = 0

# result = recursive_backtracking(init_assignment(my_csp["variables"]), my_csp)

# print(result)
# for i in reporter:
#     print(i["message"])

# X = ['1', '2', '3', '4']
# D = {'1': [1, 2, 3], '2': [2, 1, 3], '3': [3, 2, 1], '4': [1, 2, 3]}
# C = [('1', '4'), ('2', '4'), ('3', '4')]
# my_csp = {
#     "variables": X,
#     "domains": D,
#     "constraints": C
# }
# reporter = []
# counter = 0

# result = recursive_backtracking(init_assignment(my_csp["variables"]), my_csp)

# print(result)
# for i in reporter:
#     print(i["message"])
