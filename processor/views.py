import json

from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from processor.utils2 import init_assignment, recursive_backtracking

@method_decorator(csrf_exempt, name='dispatch')
class CSPView(View):
    def get(self, requests):
        return JsonResponse({"msg":"Hello World"})

    def post(self, requests):
        X = requests.POST.get("X", None)
        D = requests.POST.get("D", None)
        C = requests.POST.get("C", None)
        data = requests.POST.get("data", None)
        if data is not None:
            data = json.loads(data)
            X = [i['id'] for i in data['nodes']]
            D={}
            for i in X:
                D[i] = data["domain"]
            C = data['edges']
            csp = {
                "variables": X,
                "domains": D,
                "constraints": C
            }
            res = recursive_backtracking(init_assignment(csp["variables"]), csp)
            return JsonResponse(res)
        else:
            if X is None or D is None or C is None:
                return JsonResponse({
                    "success":False,
                    "msg":"Missing parameters"
                    })
            else:
                X = X.split(',')
                D = json.loads(D)
                C = C.split(',')
                C = [i.split(';') for i in C]
                csp = {
                    "variables": X,
                    "domains": D,
                    "constraints": C
                }
                res = recursive_backtracking(init_assignment(csp["variables"]), csp)
                return JsonResponse(res)
